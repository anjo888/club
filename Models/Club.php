<?php
/**
 * Created by PhpStorm.
 * User: farit
 * Date: 05.10.18
 * Time: 17:43
 */

class Club
{
 private   $persons = array();
 private  $currentTrack ;
 private $time;
 private $listMusic = array();

    /**
     * Club constructor.
     * @param array $persons
     * @param Music $currentTrack
     * @param $time
     * @param array $listMusic
     */
    public function __construct(array $persons, Music $currentTrack, $time, array $listMusic)
    {
        $this->persons = $persons;
        $this->currentTrack = $currentTrack;
        $this->time = $time;
        $this->listMusic = $listMusic;
    }


    /**
     * @return array
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return Music
     */
    public function getCurrentTrack()
    {
        return $this->currentTrack;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return array
     */
    public function getListMusic()
    {
        return $this->listMusic;
    }




}