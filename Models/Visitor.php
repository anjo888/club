<?php
/**
 * Created by PhpStorm.
 * User: farit
 * Date: 05.10.18
 * Time: 17:40
 */

class Visitor
{
    private $dans = array();
    private  $sex;
    private $currentStates ;

    /**
     * People constructor.
     * @param array $dans
     * @param $sex
     * @param array $currentState
     */
    public function __construct(array $dans, $sex,  $currentState)
    {
        $this->dans = $dans;
        $this->sex = $sex;
        $this->currentStates = $currentState;
    }

    /**
     * @return array
     */
    public function getDans()
    {
        return $this->dans[0];
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @return array
     */
    public function getCurrentStates()
    {
        return $this->currentStates;
    }

    public function getInfo(){

        $textSex = "Женщина:";

        if(SEX_MAN == $this->sex){
            $textSex= "Мужчина:";
        }
        return $textSex .$this->currentStates[0]->getClubAction();

    }

}