<?php
/**
 * Created by PhpStorm.
 * User: farit
 * Date: 05.10.18
 * Time: 17:45
 */

class DanceAction implements ClubAction
{

    private $type;
    private $danceText;
    private $musicTypes;

    /**
     * DanceAction constructor.
     * @param $type
     * @param $danceText
     * @param array $musicTypes
     */
    public function __construct($type, $danceText, $musicTypes)
    {
        $this->type = $type;
        $this->danceText = $danceText;
        $this->musicTypes = $musicTypes;
    }


    public function getClubAction()
    {
        return $this->danceText;
    }

    public function isActionValid(Club $club )
    {
        return in_array($club->getCurrentTrack()->getType(), $this->musicTypes);
    }

}


