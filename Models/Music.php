<?php
/**
 * Created by PhpStorm.
 * User: farit
 * Date: 05.10.18
 * Time: 17:41
 */

class Music
{

    private $type;
    private $time;

    /**
     * Music constructor.
     * @param $type
     * @param $time
     */
    public function __construct($type, $time)
    {
        $this->type = $type;
        $this->time = $time;
    }


    /**
     * @return mixed
     */
    public function getType()
    {

        $type_music = "hipHop музыка";
        if(MUSIC_TYPE_RNB == $this->type){
            $type_music= "RNB музыка";
        }elseif (MUSIC_TYPE_HOUSE == $this->type){
            $type_music= "HOUSE музыка";
        }
        return $type_music;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }



}