<?php
/**
 * Created by PhpStorm.
 * User: farit
 * Date: 05.10.18
 * Time: 17:49
 */

interface ClubAction
{
    public function getClubAction();

    public function isActionValid(Club $club);


}